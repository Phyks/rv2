Download pbf file
=================

I sure you can do that yourself


Build elevation database
========================

This database is not usefull for routing. This database is only usefull for
building routing database. This database is not needed to be generated when the
pbf is updated if the pdf cover the same area.

Analyze pbf
-----------

    ./src/analyze_pbf SOMEWHERE/france-latest.osm.pbf > SOMEWHERE/list_needed_srtm

The `analyze_pbf` program give:

 - in stdout the list of needed srtm tiles.

 - is stderr the number of couple (way,node)s usefull for routing. Let N the
   number of couple (way,node)s returned by `analyze_pbf`. The prediction are the
   following:

   - (150 Bytes) × N : the routing database size

   - (157 Bytes) × N : the minimal amount of RAM to create the database

   - (253 Bytes) × N : a /good/ amount of RAM to create the database

   - (421 Bytes) × N : the optimal amount of RAM to create the database (more
           RAM is useless).

   Keep these value in mind for next part.

Downloading SRTM tiles
----------------------

With the `list_needed_srtm`, download the tiles. A script is provided in scripts.
This script is the definition of brutality.


    ./scripts/dlsrtm SOMEWHERE/list_needed_srtm SOMEWHERE/srtmzip


Unzip SRTM files
----------------

    mkdir -p SOMEWHERE/srtm
    find SOMEWHERE/srtmzip/ -print0 | xargs -0 -n1 -I{} unzip {} -d SOMEWHERE/srtm

We don't need `SOMEWHERE/srtmzip` anymore. You can remove it.


Creating elevation db
---------------------

    ./src/elevation_create_database SOMEWHERE/ele_db_sparse SOMEWHERE/srtm/*.hgt

This elevation db is directly usable. Buto create this elevation db, the program
use a writable map, and the elevation database is a big sparse file. If you are
not planning to backup, or transfer this file, you can skip the next step, but
the time used in the next step is very small, and this is not a good idea in
general case.

We don't need `SOMEWHERE/srtm anymore`. You can remove it.

Converting elevation db
-----------------------

We use the agnostic `lmdb_rewriter` programm. It read source database, and
re-write it sequencialy (without using writable map). The two database are
equivalent, but the created elevation db from last step is a big sparse file,
and after this step, the elevation db is a small dense file.

    ./src/lmdb_rewriter SOMEWHERE/ele_db_sparse SOMEWHERE/ele_db
    
`SOMEWHERE/ele_db_sparse` is not needed anymore.


Keep it
-------

Keep this elevation database. It is not needed to recreate it when the pbf is
updated. Except the case when new area is added to the pbf which are not in the
elevation database, but in this case, you will know it, the routing db (next
part) creation will fail, with a explanation (maybe).


Testing the elevation database
------------------------------

The program `elevation_query_database` use coupe (lon,lat) in stdin and give
elevation in stdout. For example, to get the elevation of the point given by
coords (lon=1.017, lat=48.513) use:

    echo 1.017 48.513 | ./src/elevation_query_database SOMEWHERE/ele_db


Building routing database
=========================

You need to rebuild it each time the pbf is updated (if you need a updated
routing, of course). To create this database, you need to have:

 - the pbf (obviously)

 - a elevation database which cover the area

 - a large amount of RAM (see predictions in previous part obtained by pbf
  analysis)

Run:

    ./src/route_create_databases_from_pbf SOMEWHERE/france-latest.osm.pbf SOMEWHERE/ele_db SOMEWHERE/route_db

Have a cup of tea.

Testing routing database
------------------------

The routing database is made by two databases, the lookup database, and the node
database.

First we test the lookup database. For that the program
`route_query_lookup_database` do the job. This program take a set of coupe
(lon,lat) terminated by 0. For example, to get the nodes in the same connected
component which are near points of coords:
 - (lon=0.044, lat=45.661)
 - (lon=0.169, lat=45.648)
 - (lon=0.240, lat=45.737)

use the following command:

    echo 0.044 45.661 0.169 45.648 0.240 45.737 0 \
      | ./src/route_query_lookup_database SOMEWHERE/route_db

Second we test the node db. You must have node id (you can use nodes id found by
the lookup, with this you can also verify node are near queried coordinates).
For example, to obtain information about the nodes 1068822559 and 561188093, use
the following command:

    echo 1068822559 561188093 \
      | ./src/route_query_nodes_database SOMEWHERE/route_db

License of this file
====================

    Copyright (c) 2015-2016, Jean-Benoist Leger <jb@leger.tf>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

