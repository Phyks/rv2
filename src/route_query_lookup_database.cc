/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "lookup.h"
#include <iostream>

int main(int argc, char** argv)
{
    if(argc<2)
    {
        fprintf(stderr,"Usage: %s routes_db_path\nPut lon lat lon lat... and 0 on stdin\n",argv[0]);
        abort();
    }
    char zfilename[RV_ZCHAR_LENGTH];
    // lookup_db (read)
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[1],RV_LOOKUP_DB_RELATIVE_PATH);
    lookup_db_t lookup_db(zfilename);

    // nodes_db (read)
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[1],RV_NODES_DB_RELATIVE_PATH);
    nodes_db_t nodes_db(zfilename);

    std::list< std::pair<double,double> > geos;
    std::pair<double,double> current_geo;
    bool first=true;
    while(!std::cin.eof())
    {
        double read;
        
        std::cin >> read;
        if(std::cin.eof())
            break;

        if(read==0)
        {
            std::list<nid_t> nids;
            int rc = rv_lookup(
                    nodes_db,
                    lookup_db,
                    geos,
                    nids);

            if(rc!=RV_ERROR_NO_NODE_FOUND)
            {
                for(std::list<nid_t>::iterator nids_it=nids.begin();
                        nids_it!=nids.end();
                        nids_it++)
                {
                    printf("%lu ",*nids_it);
                }
            }
            else
                printf("NO NODE FOUND");
            printf("\n");

            geos.clear();
        }
        else
        {
            if(first)
                current_geo.first = read;
            else
            {
                current_geo.second = read;
                geos.push_back(current_geo);
            }
            first = !first;
        }

    }

    return(0);
}



