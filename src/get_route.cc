/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "get_route.h"

std::list<std::list<route_point_t> > get_routes(
        route_db_t* route_db,
        parameters_t & parameters,
        std::list< std::pair<double,double> > & geos,
        stdout_output_t stdout_output,
        bool state_status_on_stderr)
{
    std::list<nid_t> nids;

    if(state_status_on_stderr)
        fprintf(stderr,"## state: lookup\n");
    int rc = route_db->lookup(geos, nids);

    if(rc == RV_ERROR_NO_NODE_FOUND)
    {
        fprintf(stderr,"## state: failed no_nodes_found_in_the_same_cc\n");
        abort();
    }

    std::list<std::list<route_point_t> > ret;
    unsigned int track = 0;

    for(std::list<nid_t>::iterator nids_it = nids.begin();
            nids_it != nids.end();
            nids_it++)
    {
        std::list<nid_t>::iterator nids_it_next = nids_it;
        nids_it_next++;
        if(nids_it_next!=nids.end())
        {
            if(state_status_on_stderr)
                fprintf(stderr,"## state: routing_track %lu\n",track);

            std::list<route_point_t> current_route =
                    get_route_between_nids(
                        route_db,
                        parameters,
                        *nids_it,
                        *nids_it_next,
                        state_status_on_stderr);
            ret.push_back(current_route);

            if(stdout_output == RV_STDOUT_OUTPUT_TEXT)
                write_to_stdout(route_db, track, current_route);
        }
        track++;
    }
    return(ret);
}


std::list<route_point_t> get_route_between_nids(
        route_db_t* route_db,
        parameters_t & parameters,
        nid_t & nid_initial,
        nid_t & nid_final,
        bool state_status_on_stderr)
{
    std::priority_queue<edge_in_queue_t> queue; // This is the ordoned queue used to explore
    edge_map_t edges_map;


    // first we add edge starting from the initial node in the queue
    node_info_t* initial_node = route_db->get_node(nid_initial);
    node_info_t* final_node = route_db->get_node(nid_final);
 
    double total_flight_distance = rv_distance(
            initial_node->fixed->lon,
            initial_node->fixed->lat,
            final_node->fixed->lon,
            final_node->fixed->lat);
    unsigned int last_printed_status = 0;

    edge_t null_edge;
    null_edge.A = 0;
    null_edge.B = 0;
    for(unsigned int i=0; i< initial_node->fixed->neighbors_number; i++)
    {
        edge_t edge;
        edge.A=nid_initial;
        edge.B=initial_node->neighbors[i].to;
        status_t & status = edges_map[edge];
        status.criterion=0;
        status.velocity=0;
        status.energy=0;
        status.time=0;
        status.distance=0;
        status.power=0;
        status.walk=false;
        status.from_edge=null_edge;
        status.freezed=false;
        status.initialized=true;
        edge_in_queue_t edge_in_queue;
        edge_in_queue.edge = edge;
        edge_in_queue.heuristic = rv_heuristic(parameters, initial_node->fixed, final_node->fixed, status);
        queue.push(edge_in_queue);
    }

    while(!queue.empty())
    {
        // We take the first
        edge_in_queue_t running_edge_in_queue = queue.top();
        queue.pop();

        status_t & running_status = edges_map[running_edge_in_queue.edge];

        if(running_status.freezed) // We have already foud the optimal for this edge
        {
            continue;
        }

        running_status.freezed = true;
       
        
        if(running_edge_in_queue.edge.A == nid_final) // It is finish
            break;

        node_info_t* A_node = route_db->get_node(running_edge_in_queue.edge.A);
        node_info_t* B_node = route_db->get_node(running_edge_in_queue.edge.B);

        if(state_status_on_stderr)
        {
            double flight_distance = rv_distance(
                    final_node->fixed->lon,
                    final_node->fixed->lat,
                    B_node->fixed->lon,
                    B_node->fixed->lat);
            double status = 1 - flight_distance / total_flight_distance;
            status = status*status;
            if( (unsigned int)(status*RV_STATUS_SCALE) > last_printed_status)
            {
                last_printed_status = (unsigned int)(status*RV_STATUS_SCALE);
                fprintf(stderr,"## status: %lu\n",last_printed_status);
            }
        }



        if(running_edge_in_queue.edge.B == nid_final) // We are near the end
        {
            edge_in_queue_t following_edge_in_queue;
            following_edge_in_queue.edge.A = nid_final;
            following_edge_in_queue.edge.B = 0;
            status_t & following_status = edges_map[following_edge_in_queue.edge];
            status_t proposed_following_status;

            // We lookup the way_kind
            way_kind_t way_kind;
            for(unsigned int i=0;i<B_node->fixed->neighbors_number; i++)
            {
                if(B_node->neighbors[i].from == running_edge_in_queue.edge.A)
                {
                    way_kind = B_node->neighbors[i].way_kind;
                    break;
                }
            }

            rv_step(
                    parameters,
                    A_node->fixed,
                    B_node->fixed,
                    NULL,
                    way_kind,
                    running_status,
                    proposed_following_status);

            if(!following_status.initialized 
                    || proposed_following_status.criterion < following_status.criterion)
            {
                following_status = proposed_following_status;
                following_status.from_edge=running_edge_in_queue.edge;
                following_status.initialized=true;
                following_edge_in_queue.heuristic = rv_heuristic(
                        parameters,
                        B_node->fixed,
                        final_node->fixed,
                        following_status);
                queue.push(following_edge_in_queue);
            }
            continue;
        }

        for(unsigned int i=0; i<B_node->fixed->neighbors_number; i++)
        {
            if(B_node->neighbors[i].from == running_edge_in_queue.edge.A)
            {
                // We explore the following
                nid_t & nid_C = B_node->neighbors[i].to;
                if(!nid_C) // This is a dead end and we are not at the end
                    continue;
                way_kind_t & way_kind = B_node->neighbors[i].way_kind;

                node_info_t* C_node = route_db->get_node(nid_C);

                edge_in_queue_t following_edge_in_queue;
                following_edge_in_queue.edge.A = running_edge_in_queue.edge.B;
                following_edge_in_queue.edge.B = nid_C;
                status_t proposed_following_status;
                status_t & following_status = edges_map[following_edge_in_queue.edge];

                rv_step(
                        parameters,
                        A_node->fixed,
                        B_node->fixed,
                        C_node->fixed,
                        way_kind,
                        running_status,
                        proposed_following_status);

                if(!following_status.freezed
                        && ( !following_status.initialized 
                            || proposed_following_status.criterion < following_status.criterion))
                {
                    following_status = proposed_following_status;
                    following_status.from_edge=running_edge_in_queue.edge;
                    following_status.initialized=true;
                    following_edge_in_queue.heuristic = rv_heuristic(
                            parameters,
                            B_node->fixed,
                            final_node->fixed,
                            following_status);
                    queue.push(following_edge_in_queue);
                }
            }
        }
    }

    edge_t edge;
    edge.A = nid_final;
    edge.B =0;

    status_t status = edges_map[edge];
    if(!status.freezed) // FUCK YOU
    {
        fprintf(stderr,"Queue finished with last edge not freezed\n");
        abort();
    }

    // We only have to follow the edges in opposite order
    std::list<route_point_t> ret;
    while(true)
    {
        route_point_t routing_point;
        routing_point.nid = edge.A;
        routing_point.velocity = status.velocity;
        routing_point.distance = status.distance;
        routing_point.energy = status.energy;
        routing_point.time = status.time;
        routing_point.walk = status.walk;
        routing_point.power = status.power;
        ret.push_front(routing_point);

        if(edge.A == nid_initial)
            break;

        edge = status.from_edge;
        status = edges_map[edge];
    }

    return(ret);
}

