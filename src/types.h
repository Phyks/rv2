/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef H_TYPES
#define H_TYPES 1

#include "elevation.h"
#include <stdint.h>
#include <unordered_map>
#include <map>

typedef uint64_t nid_t;
typedef uint64_t wid_t;
typedef uint64_t ccid_t;

struct ncc_t
{
    nid_t nid;
    ccid_t ccid;
};

typedef struct ncc_t ncc_t;

enum way_kind_t
{
    RV_WK_FOOTWAY,
    RV_WK_CYCLEWAY,
    RV_WK_OTHERS
};

typedef enum way_kind_t way_kind_t;

struct neighbor_t
{
    nid_t from;
    nid_t to;
    way_kind_t way_kind;
};

typedef struct neighbor_t neightbor_t;

struct node_info_fixed_t
{
    double lon;
    double lat;
    elevation_value elevation;
    uint32_t neighbors_number;
};

typedef struct node_info_fixed_t node_info_fixed_t;

struct node_info_t
{
    node_info_fixed_t* fixed;
    neighbor_t* neighbors;
};

typedef struct node_info_t node_info_t;

enum stdout_output_t
{
    RV_STDOUT_OUTPUT_NONE,
    RV_STDOUT_OUTPUT_TEXT
};

typedef enum stdout_output_t stdout_output_t;

enum criterion_t
{
    RV_CRITERION_ENERGY,
    RV_CRITERION_TIME,
    RV_CRITERION_DISTANCE,
    RV_CRITERION_ENERGY_D,
    RV_CRITERION_TIME_D
};

typedef enum criterion_t criterion_t;

struct parameters_t
{
    double mass;
    double power;
    double SCx;
    double Cr;
    double velocity_nopower;
    double velocity_brake;
    double velocity_equilibrium;
    criterion_t criterion;
    double power_walk;
    double lateral_acceleration;
    double walk_penalty;
    double Cw;
};

typedef struct parameters_t parameters_t;

struct edge_t
{
    nid_t A;
    nid_t B;

    bool operator<(const edge_t & rhs) const
    {
        if(A<rhs.A)
            return true;
        if(A>rhs.A)
            return false;
        return B<rhs.B;
    }
};

typedef struct edge_t edge_t;

struct status_t
{
    double criterion;
    double velocity;
    double distance;
    double energy;
    double time;
    bool walk;
    edge_t from_edge;
    bool freezed;
    bool initialized;
    double power;

    status_t() : freezed(false), initialized(false) {}
};

typedef struct status_t status_t;

struct route_point_t
{
    nid_t nid;
    double velocity;
    double distance;
    double energy;
    double time;
    double power;
    bool walk;
};

typedef struct route_point_t route_point_t;

struct edge_in_queue_t
{
    edge_t edge;
    double heuristic;

    // Warning lower the criterion is higher the priority is
    bool operator<(const edge_in_queue_t & rhs) const { return heuristic > rhs.heuristic; }
};
typedef struct edge_in_queue_t edge_in_queue_t;

struct edge_map_t
{
    std::map<nid_t, std::map<nid_t,status_t>> map;
    status_t & operator[]( const edge_t & edge ) { return map[edge.A][edge.B]; }
};

#endif
