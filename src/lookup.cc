/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "lookup.h"
#include <map>

class lookup_dist_t
{
    public:
    double value;
    nid_t nid;
    lookup_dist_t() : value(RV_EARTH_RADIUS) {}
};

int rv_lookup(
        nodes_db_t & nodes_db,
        lookup_db_t & lookup_db,
        std::list< std::pair<double,double> > & geos,
        std::list<nid_t> & nids)
{
    std::map<ccid_t, unsigned int> map_of_touch_cc;
    std::list< std::map<ccid_t,lookup_dist_t> > list_of_founds;

    for(std::list< std::pair<double,double> >::iterator geos_it = geos.begin();
            geos_it!=geos.end();
            geos_it++)
    {
        std::list<ncc_t> area;

        int32_t key_lon = (int32_t)((180+(*geos_it).first)*RV_LOOKUP_PREC);
        int32_t key_lat = (int32_t)((180+(*geos_it).second)*RV_LOOKUP_PREC);

        for(int i=-1; i<=1; i++)
        {
            for(int j=-1; j<=1; j++)
            {
                lookup_key_t lookup_key;
                lookup_key.lon = key_lon+i;
                lookup_key.lat = key_lat+j;
                lookup_db.get_area(lookup_key, area);
            }
        }

        if(area.size()==0)
        {
            fprintf(stderr,"No node found near coords (%f,%f)\n",(*geos_it).first,(*geos_it).second);
            return RV_ERROR_NO_NODE_FOUND;
        }

        // map on cc->(dist,nid)
        std::map<ccid_t,lookup_dist_t> founds_cc;

        for(std::list<ncc_t>::iterator area_it = area.begin();
                area_it!=area.end();
                area_it++)
        {
            node_info_t ni;
            nodes_db.get_node((*area_it).nid,&ni);
            double distance = rv_distance(
                    (*geos_it).first,
                    (*geos_it).second,
                    ni.fixed->lon,
                    ni.fixed->lat);
            
            lookup_dist_t & cc_in_map = founds_cc[(*area_it).ccid];
            if(cc_in_map.value>distance)
            {
                cc_in_map.value = distance;
                cc_in_map.nid = (*area_it).nid;
            }
        }

        list_of_founds.push_back(founds_cc);

        for(std::map<ccid_t,lookup_dist_t>::iterator founds_cc_it = founds_cc.begin();
                founds_cc_it!=founds_cc.end();
                founds_cc_it++)
        {
            map_of_touch_cc[ founds_cc_it->first ] +=1;
        }
    }

    // Now we will look all CC which are near all points
    std::list<ccid_t> allowed_cc;

    for(std::map<ccid_t, unsigned int>::iterator map_of_touch_cc_it=map_of_touch_cc.begin();
            map_of_touch_cc_it!=map_of_touch_cc.end();
            map_of_touch_cc_it++)
    {
        if(map_of_touch_cc_it->second == geos.size())
        {
            allowed_cc.push_back(map_of_touch_cc_it->first);
        }
    }

    // Now we find the CC in allowed CC which minimize the total sum projection
    // distance
    double min_proj_dist = RV_EARTH_RADIUS;
    ccid_t min_ccid = 0;
    for(std::list<ccid_t>::iterator allowed_cc_it = allowed_cc.begin();
            allowed_cc_it!=allowed_cc.end();
            allowed_cc_it++)
    {
        ccid_t ccid = *allowed_cc_it;

        double proj_dist=0;
        for(std::list< std::map<ccid_t,lookup_dist_t> >::iterator list_of_founds_it=list_of_founds.begin();
                list_of_founds_it!=list_of_founds.end();
                list_of_founds_it++)
        {
            proj_dist += (*list_of_founds_it)[ccid].value;
        }

        if(proj_dist<min_proj_dist)
        {
            min_proj_dist = proj_dist;
            min_ccid = ccid;
        }
    }
    
    nids.clear();
    for(std::list< std::map<ccid_t,lookup_dist_t> >::iterator list_of_founds_it=list_of_founds.begin();
            list_of_founds_it!=list_of_founds.end();
            list_of_founds_it++)
    {
        nids.push_back( (*list_of_founds_it)[min_ccid].nid );
    }

    return(0);

}




            





        







