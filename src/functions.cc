/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "functions.h"

double rv_distance(
        double & lon1,
        double & lat1,
        double & lon2,
        double & lat2)
{
    double diffx = (lon1-lon2)/180*RV_PI*RV_EARTH_RADIUS*std::cos((lat1+lat2)/360*RV_PI);
    double diffy = (lat1-lat2)/180*RV_PI*RV_EARTH_RADIUS;
    double dist = std::sqrt( diffx*diffx + diffy*diffy );
    return dist;
}

double rv_distance(
        double & lon1,
        double & lat1,
        elevation_value & ele1,
        double & lon2,
        double & lat2,
        elevation_value & ele2)
{
    double diffx = (lon1-lon2)/180*RV_PI*RV_EARTH_RADIUS*std::cos((lat1+lat2)/360*RV_PI);
    double diffy = (lat1-lat2)/180*RV_PI*RV_EARTH_RADIUS;
    double diffz = ele1-ele2;
    double dist = std::sqrt( diffx*diffx + diffy*diffy + diffz*diffz);
    return dist;
}

void rv_do_with_velocity(
        parameters_t & parameters,
        double & velocityA,
        double & velocityB,
        double & delta_elevation,
        double & distance_AB,
        double & energy,
        double & power,
        double & time)
{
    // This function write the energy, avg power, and time from A to B, with
    // given velocity
    double delta_kinetic_energy = .5 * parameters.mass * ( velocityB * velocityB - velocityA * velocityA);
    double delta_gravitational_potential_energy = parameters.mass * RV_G * delta_elevation;
    double work_of_rolling_resistance = parameters.mass * RV_G * parameters.Cr * distance_AB;
    double work_of_air_resistance = .25 * RV_RHO_AIR * parameters.SCx * distance_AB * ( velocityB * velocityB + velocityA * velocityA );
    
    energy = delta_kinetic_energy + delta_gravitational_potential_energy + work_of_rolling_resistance + work_of_air_resistance;
    time = 2*distance_AB/(velocityA+velocityB);
    power = energy / time;
    if(energy<0) // In fact we brake
        energy=0;
}

void rv_do_with_null_power(
        parameters_t & parameters,
        double & velocityA,
        double & velocityB,
        double & delta_elevation,
        double & distance_AB,
        double & energy,
        double & power,
        double & time)
{
    double delta_gravitational_potential_energy = parameters.mass * RV_G * delta_elevation;
    double work_of_rolling_resistance = parameters.mass * RV_G * parameters.Cr * distance_AB;

    velocityB = std::sqrt(((2*parameters.mass-RV_RHO_AIR*parameters.SCx*distance_AB) * velocityA * velocityA - 4*delta_gravitational_potential_energy -4*work_of_rolling_resistance)/(2*parameters.mass+RV_RHO_AIR*parameters.SCx*distance_AB));
    energy = 0;
    power = 0;
    time = 2*distance_AB/(velocityA+velocityB);
}

void rv_do_walk(
        parameters_t & parameters,
        double & velocityA,
        double & velocityB,
        double & delta_elevation,
        double & distance_AB,
        double & energy,
        double & power,
        double & time)
{
    double delta_gravitational_potential_energy=0;
    if(delta_elevation>0)
        delta_gravitational_potential_energy = parameters.mass * RV_G * delta_elevation;
    double work_of_walking_resistance = parameters.mass * RV_G * parameters.Cw * distance_AB;
    energy = delta_gravitational_potential_energy + work_of_walking_resistance;
    time = energy / parameters.power_walk;
    velocityB = 0;
    power = parameters.power_walk;
}

void order3_poly(double & a, double & b, double & c, double & d, double & root3)
{
    double p = (3*a*c-b*b)/(3*a*a); // velocity^2
    double q = (2*b*b*b-9*a*b*c+27*a*a*d)/(27*a*a*a); // velocity^3
    double e = -b/(3*a); // velocity

    double discri = 27*q*q+4*p*p*p; // velocity^6
    
    // We solve the poly vB**3 + p*vB + q = 0
    // done with maxima
    
    if(discri>0)
    {
        // done by assume with maxima and realpart(solve(,)[3])
        // we know the solution is real, but realpart is to force the
        // computation without using imagnary computation
        double three_power_ = std::pow(3.0,3.0/2.0);
        double two_power_ = std::pow(2.0,1.0/3.0);
        root3 = (cos(atan2(0,(sqrt(discri)-three_power_*q)/(2*three_power_))/3)*std::pow(sqrt(discri)-three_power_*q,2.0/3.0)-two_power_*two_power_*cos(atan2(0,(sqrt(discri)-three_power_*q)/(2*three_power_))/3)*p)/(two_power_*sqrt(3)*std::pow(abs(sqrt(discri)-three_power_*q),1.0/3.0));
    }
    else
    {
        // idem but we assume <0
        double three_power_ = std::pow(3.0,3.0/2.0);
        root3 = -(2*p*cos(atan2(sqrt(-discri)/(2*three_power_),-q/2)/3))/(sqrt(-3*p));
    }
    root3+=e;
    
    if(root3<0)
        root3=0;
    if(isnan(root3))
        root3=0;
}

double rv_constant_power_biking(
        parameters_t & parameters, 
        double & velocityA, 
        double & delta_elevation, 
        double & distance_AB)
{
    double velocityB;
    double delta_gravitational_potential_energy = parameters.mass * RV_G * delta_elevation;
    double work_of_rolling_resistance = parameters.mass * RV_G * parameters.Cr * distance_AB;

    // we write a poly a*vB^3+b*vB^2+c*vB+d
    // the root is the answer
    double a = (RV_RHO_AIR*parameters.SCx*distance_AB+2*parameters.mass)/4;
    double b = a*velocityA;
    double c = (4*work_of_rolling_resistance+(RV_RHO_AIR*parameters.SCx*distance_AB-2*parameters.mass)*velocityA*velocityA+4*delta_gravitational_potential_energy)/4;
    double d = c*velocityA - 2*distance_AB*parameters.power;

    order3_poly(a,b,c,d,velocityB);

    // Now hack
    // To avoid oscillation starting from 0, we choose the speed half of old and
    // theoritical new
    velocityB = .9*velocityB + .1*velocityA;


    return(velocityB);
}

double rv_constant_speed_with_constant_power(
        parameters_t & parameters,
        double & distance,
        double & delta_elevation)
{
    double velocity;
    double a = -(RV_RHO_AIR*parameters.SCx)/2; // kg.m^(-1)
    double b = 0;
    double c = -(delta_elevation*RV_G*parameters.mass)/distance-parameters.Cr*RV_G*parameters.mass; // kg.m.s^(-2)
    double d = parameters.power; // kg.m^2.s^(-3)

    order3_poly(a,b,c,d,velocity);
    return(velocity);
}

double rv_heuristic(
        parameters_t & parameters,
        node_info_fixed_t *nodeA,
        node_info_fixed_t *nodeF,
        status_t & statusA)
{
    if(parameters.criterion == RV_CRITERION_ENERGY_D || parameters.criterion == RV_CRITERION_TIME_D)
        return(statusA.criterion);

    double distance_AF = rv_distance(
                                    nodeA->lon,
                                    nodeA->lat,
                                    nodeA->elevation,
                                    nodeF->lon,
                                    nodeF->lat,
                                    nodeF->elevation);

    if(parameters.criterion == RV_CRITERION_DISTANCE)
    {
        return(statusA.criterion + distance_AF);
    }

    if(parameters.criterion == RV_CRITERION_ENERGY || parameters.criterion == RV_CRITERION_TIME)
    {
        double delta_elevation = nodeF->elevation - nodeA->elevation;
        double velocity = rv_constant_speed_with_constant_power(parameters, distance_AF, delta_elevation);
        double min_energy = parameters.mass * RV_G * (parameters.Cr * distance_AF + delta_elevation) + .5*RV_RHO_AIR*parameters.SCx*velocity*velocity*distance_AF;
        if(min_energy<0)
            min_energy = 0;

        if(parameters.criterion == RV_CRITERION_ENERGY)
            return(statusA.criterion + min_energy);
        else
            return(statusA.criterion + min_energy/parameters.power);
    }

    // What the hell are we doing here
    return(0);
}


void rv_step(
        parameters_t & parameters,
        node_info_fixed_t *nodeA,
        node_info_fixed_t *nodeB,
        node_info_fixed_t *nodeC,
        way_kind_t way_kindAB,
        status_t & statusA,
        status_t & statusB)
{
    double distance_AB, distance_BC, distance_AC;
    double curvature_maximum_velocity=0;

    distance_AB = rv_distance(
                             nodeA->lon,
                             nodeA->lat,
                             nodeA->elevation,
                             nodeB->lon,
                             nodeB->lat,
                             nodeB->elevation);
    if(nodeC)
    {
        distance_BC = rv_distance(
                                 nodeB->lon,
                                 nodeB->lat,
                                 nodeB->elevation,
                                 nodeC->lon,
                                 nodeC->lat,
                                 nodeC->elevation);
        distance_AC = rv_distance(
                                 nodeA->lon,
                                 nodeA->lat,
                                 nodeA->elevation,
                                 nodeC->lon,
                                 nodeC->lat,
                                 nodeC->elevation);

        double angle = std::acos((distance_AB*distance_AB + distance_BC*distance_BC - distance_AC*distance_AC)/(2*distance_AB*distance_BC));
        double way_width = (way_kindAB == RV_WK_CYCLEWAY) ? RV_WIDTH_CYCLEWAY : RV_WIDTH_OTHERS;
        if(distance_AB<2*way_width)
            way_width = distance_AB/2;
        if(distance_BC<2*way_width)
            way_width = distance_BC/2;
        double radius_of_curvature;
        if(angle >= RV_MAXIMUM_USED_ANGLE)
            radius_of_curvature = RV_MAXIMUM_RADIUS_OF_CURVATURE;
        else
            radius_of_curvature = way_width / (1.0 - std::sin(.5*angle));

        curvature_maximum_velocity = std::sqrt(parameters.lateral_acceleration * RV_G * radius_of_curvature);

    }

    double & velocityA = statusA.velocity;
    double & velocityB = statusB.velocity;
    double delta_elevation = nodeB->elevation - nodeA->elevation;

    // first we consider we biking with the constant power
    if(nodeC)
        velocityB = rv_constant_power_biking(parameters, velocityA, delta_elevation, distance_AB);
    else
        velocityB = 0; // We stop in nodeB

    double energy, power, time;
    rv_do_with_velocity(parameters, velocityA, velocityB, delta_elevation, distance_AB, energy, power, time);
    statusB.walk = false;
    double penalty=1;

    if(velocityB > parameters.velocity_nopower) // In fact we reduce the power to fit te velocity_nopower
    {
        velocityB = parameters.velocity_nopower;
        rv_do_with_velocity(parameters, velocityA, velocityB, delta_elevation, distance_AB, energy, power, time);

        if(power <= 0) // In fact we don't need power to have this velocity
        {
            rv_do_with_null_power(parameters, velocityA, velocityB, delta_elevation, distance_AB, energy, power, time);
            
            if(velocityB > parameters.velocity_brake) // In fact we need to brake
            {
                velocityB = parameters.velocity_brake;
                rv_do_with_velocity(parameters, velocityA, velocityB, delta_elevation, distance_AB, energy, power, time);
            }
        }
    }

    if(velocityB > curvature_maximum_velocity) // We need to reduce the power (or brake) to respect the maximum speed of curvature
    {
        velocityB = curvature_maximum_velocity;
        rv_do_with_velocity(parameters, velocityA, velocityB, delta_elevation, distance_AB, energy, power, time);
    }

    if(way_kindAB == RV_WK_FOOTWAY || .5*(velocityA+velocityB)<parameters.velocity_equilibrium)
    {
        // We WALK !
        rv_do_walk(parameters, velocityA, velocityB, delta_elevation, distance_AB, energy, power, time);
        statusB.walk = true;
        penalty = parameters.walk_penalty;
    }

    statusB.energy = statusA.energy + energy;
    statusB.time = statusA.time + time;
    statusB.distance = statusA.distance + distance_AB;
    statusB.power = power;

    if(parameters.criterion == RV_CRITERION_ENERGY || parameters.criterion == RV_CRITERION_ENERGY_D)
        statusB.criterion = statusA.criterion + energy * penalty;
    else if(parameters.criterion == RV_CRITERION_TIME || parameters.criterion == RV_CRITERION_TIME_D)
        statusB.criterion = statusA.criterion + time * penalty;
    else if(parameters.criterion == RV_CRITERION_DISTANCE)
        statusB.criterion = statusA.criterion + distance_AB * penalty;
}
