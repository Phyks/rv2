/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "parse_way.h"
#include <iostream>
#include <osmpbf/osmfile.h>
#include <osmpbf/inode.h>
#include <osmpbf/iway.h>
#include <osmpbf/irelation.h>
#include <osmpbf/filter.h>
#include <osmpbf/primitiveblockinputadaptor.h>
#include <algorithm>
#include "conv_functions.h"
#include "elevation.h"
#include "types.h"
#include "nodes_db.h"
#include "lookup_db.h"
#include <sys/stat.h>
#include <sys/types.h>



struct simple_way_t
{
    std::vector<nid_t> nids;
    int oneway;
    way_kind_t way_kind;
};

struct simple_node_t
{
    ccid_t ccid;
    double lon;
    double lat;
    elevation_value elevation;
};

void parseBlock_ways(
        osmpbf::PrimitiveBlockInputAdaptor & pbi,
        std::unordered_map<wid_t, simple_way_t> & ways,
        std::unordered_map<nid_t, std::vector<wid_t>> & node_ways)
{
		
	if (pbi.waysSize())
    {
		for (osmpbf::IWayStream way = pbi.getWayStream(); !way.isNull(); way.next())
        {
            std::string tag_highway, tag_cycleway, tag_bicycle, tag_oneway, tag_junction;
			for(uint32_t i = 0, s = way.tagsSize();  i < s; ++i)
            {
                if( way.key(i) == "highway" )
                    tag_highway = way.value(i);
                if( way.key(i) == "cycleway" )
                    tag_cycleway = way.value(i);
                if( way.key(i) == "bicycle" )
                    tag_bicycle = way.value(i);
                if( way.key(i) == "oneway" )
                    tag_oneway = way.value(i);
                if( way.key(i) == "junction" )
                    tag_junction = way.value(i);
			}

            bool we_use_it;
            way_kind_t way_kind;
            int oneway;

            parse_way(
                    we_use_it,
                    way_kind,
                    oneway,
                    tag_highway,
                    tag_cycleway,
                    tag_bicycle,
                    tag_oneway,
                    tag_junction);
    
            if(we_use_it)
            {
                simple_way_t simple_way;
                simple_way.oneway = oneway;
                simple_way.way_kind = way_kind;
                for(osmpbf::IWay::RefIterator refIt(way.refBegin()), refEnd(way.refEnd()); refIt != refEnd; ++refIt)
                {
                    node_ways[*refIt].push_back(way.id());
                    simple_way.nids.push_back(*refIt);
                }

                ways[way.id()] = simple_way;
            }
		}
	}	
}

void parseBlock_nodes(
        osmpbf::PrimitiveBlockInputAdaptor & pbi,
        elevation & elevation_db,
        nodes_db_t & nodes_db,
        std::unordered_map<wid_t, simple_way_t> & ways,
        std::unordered_map<wid_t,ccid_t> & ways_ccid,
        std::unordered_map<nid_t, std::vector<wid_t>> & node_ways,
        std::unordered_map<int32_t,std::unordered_map<int32_t,std::list<ncc_t>>> & lookup)
{
	
	if (pbi.nodesSize())
    {
		for (osmpbf::INodeStream node = pbi.getNodeStream(); !node.isNull(); node.next())
        {
            // is the node exist in our database
            if(node_ways.count(node.id())>0)
            {
                double lon = node.lond();
                double lat = node.latd();
                elevation_value elevation = elevation_db.get_elevation(lon,lat);
                nid_t nid = node.id();
                ccid_t ccid;
            
                std::vector<wid_t> & ways_of_nodes = node_ways[nid];
                std::list<std::pair<nid_t,way_kind_t>> neighbors_from;
                std::list<nid_t> neighbors_to;


                for(auto way_it = ways_of_nodes.begin();
                        way_it != ways_of_nodes.end();
                        way_it++)
                {
                    simple_way_t & way = ways[*way_it];
                    ccid = ways_ccid[*way_it];
                    for(unsigned int i = 0; i<way.nids.size(); i++)
                    {
                        if(way.nids[i] == nid)
                        {
                            if(i>0)
                            {
                                if(way.oneway>=0)
                                {
                                    // i-1 from
                                    neighbors_from.push_back( std::pair<nid_t,way_kind_t>(way.nids[i-1],way.way_kind) );
                                }

                                if(way.oneway<=0)
                                {
                                    // i-1 to
                                    neighbors_to.push_back( way.nids[i-1] );
                                }
                            }

                            if(i<way.nids.size()-1)
                            {
                                if(way.oneway>=0)
                                {
                                    // i+1 to
                                    neighbors_to.push_back( way.nids[i+1] );
                                }

                                if(way.oneway<=0)
                                {
                                    // i+1 from
                                    neighbors_from.push_back( std::pair<nid_t,way_kind_t>(way.nids[i+1],way.way_kind) );
                                }
                            }
                        }
                    }
                }

                std::list<neighbor_t> neighbors;
                for(std::list< std::pair<nid_t,way_kind_t> >::iterator neighbors_from_it = neighbors_from.begin();
                        neighbors_from_it!=neighbors_from.end();
                        neighbors_from_it++)
                {
                    nid_t nid_from = (*neighbors_from_it).first;
                    way_kind_t way_kind = (*neighbors_from_it).second;

                    for(std::list< nid_t >::iterator neighbors_to_it = neighbors_to.begin();
                            neighbors_to_it!=neighbors_to.end();
                            neighbors_to_it++)
                    {
                        nid_t nid_to = *neighbors_to_it;

                        if(nid_from!=nid_to)
                        {
                            neighbor_t neighbor;
                            neighbor.from = nid_from;
                            neighbor.to = nid_to;
                            neighbor.way_kind = way_kind;
                            neighbors.push_back(neighbor);
                        }
                    }
                }

                if(neighbors.size()==0)
                {
                    // No-exit way, last node
                    for(std::list< std::pair<nid_t,way_kind_t> >::iterator neighbors_from_it = neighbors_from.begin();
                            neighbors_from_it!=neighbors_from.end();
                            neighbors_from_it++)
                    {
                        nid_t nid_from = (*neighbors_from_it).first;
                        way_kind_t way_kind = (*neighbors_from_it).second;

                        neighbor_t neighbor;
                        neighbor.from = nid_from;
                        neighbor.to = 0;
                        neighbor.way_kind = way_kind;
                        neighbors.push_back(neighbor);
                    }

                    for(std::list< nid_t >::iterator neighbors_to_it = neighbors_to.begin();
                            neighbors_to_it!=neighbors_to.end();
                            neighbors_to_it++)
                    {
                        nid_t nid_to = *neighbors_to_it;

                        neighbor_t neighbor;
                        neighbor.from = 0;
                        neighbor.to = nid_to;
                        neighbor.way_kind = RV_WK_OTHERS;
                        neighbors.push_back(neighbor);
                    }
                }
                
                nodes_db.write_node(
                        nid,
                        lon,
                        lat,
                        elevation,
                        neighbors);

                int32_t key_lon = (int32_t)((180+lon)*RV_LOOKUP_PREC);
                int32_t key_lat = (int32_t)((180+lat)*RV_LOOKUP_PREC);

                ncc_t ncc;
                ncc.nid = nid;
                ncc.ccid = ccid;
                lookup[key_lon][key_lat].push_back(ncc);
            }
		}
	}
}


int main(int argc, char ** argv)
{
	if (argc < 4)
    {
		fprintf(stderr,"Usage: %s pbf elevation_db route_db\n",argv[0]);
        abort();
	}
    
    char zreq[RV_ZCHAR_LENGTH];
    char zfilename[RV_ZCHAR_LENGTH];

    // cleaning (without checking status, lmdb will check after and fail if this
    // is not goot)

    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/data.mdb",argv[3],RV_NODES_DB_RELATIVE_PATH);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/lock.mdb",argv[3],RV_NODES_DB_RELATIVE_PATH);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/data.mdb",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/lock.mdb",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    remove(zfilename);
    mkdir(argv[3],0755);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_NODES_DB_RELATIVE_PATH);
    mkdir(zfilename,0755);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    mkdir(zfilename,0755);

    
    std::unordered_map<wid_t, simple_way_t> ways;
    std::unordered_map<nid_t, std::vector<wid_t>> node_ways;

    fprintf(stderr,"Parsing ways from pbf and creating ways map\n");
    // scope
    {    
        std::string inputFileName(argv[1]);

        osmpbf::OSMFileIn inFile(inputFileName, false);

        if (!inFile.open())
        {
            std::cout << "Failed to open " <<  inputFileName << std::endl;
            return -1;
        }

        osmpbf::PrimitiveBlockInputAdaptor pbi;
        while (inFile.parseNextBlock(pbi))
        {
            if (pbi.isNull())
                continue;
            parseBlock_ways(pbi, ways, node_ways);
        }
    }

    fprintf(stderr,"Computing connected components\n");
    // now we construct way_way adjacency and map way_ccid
    std::unordered_map<wid_t,ccid_t> ways_ccid;
    {
        std::unordered_map<wid_t,std::vector<wid_t>> way_ways;

        for(auto ways_it = ways.begin();
                ways_it != ways.end();
                ways_it++)
        {
            // ways_it->first wid
            std::vector<wid_t> & neighbors = way_ways[ways_it->first];
            ways_ccid[ways_it->first] = ways_it->first;
            // ways_it->second.nids nodes
            for(auto nit = ways_it->second.nids.begin();
                    nit != ways_it->second.nids.end();
                    nit ++)
            {
                // we get ways
                std::vector<wid_t> & node_neighbors = node_ways[*nit];
                for(auto node_neighbors_it = node_neighbors.begin();
                        node_neighbors_it != node_neighbors.end();
                        node_neighbors_it++)
                {
                    if(*node_neighbors_it != ways_it->first)
                        neighbors.push_back(*node_neighbors_it);
                }
            }

            std::sort(neighbors.begin(),neighbors.end());
            auto last = std::unique(neighbors.begin(),neighbors.end());
            neighbors.erase(last,neighbors.end());

        }

        unsigned int iter = 0;
        while(1)
        {
            unsigned int modified;

            modified = 0;
            iter++;

            // first we build a ccid equivalence map
            std::unordered_map<ccid_t,ccid_t> equivalence;
            for(auto ways_it = ways_ccid.begin();
                    ways_it != ways_ccid.end();
                    ways_it++)
            {

                ccid_t & ccid = equivalence[ways_it->second];
                if(ccid==0) // first we see it
                    ccid = ways_it->second;
                std::vector<wid_t> & neighbors = way_ways[ways_it->first];
                for(unsigned int t=0;t<neighbors.size();t++)
                {
                    ccid_t & neigh_ccid = ways_ccid[neighbors[t]];
                    if(neigh_ccid < ccid)
                        ccid = neigh_ccid;
                }
            }

            // no we apply the equivalence map on all ways
            for(auto ways_it = ways_ccid.begin();
                    ways_it != ways_ccid.end();
                    ways_it++)
            {
                ccid_t & ccid = equivalence[ways_it->second];
                if(ccid != ways_it->second)
                {
                    modified++;
                    ways_it->second = ccid;
                }
            }


            RV_BLANKLINE;
            fprintf(stderr,"\r-> Pass %u done: Modified %u ",iter,modified);
            if(modified==0)
                break;
        }
        RV_BLANKLINE;
    }


    fprintf(stderr,"Parsing nodes from pbf computing elevation and writing nodes preparing also lookup\n");
    // scope
    
    std::unordered_map<int32_t,std::unordered_map<int32_t,std::list<ncc_t>>> lookup;
    
    {
        elevation elevation_db(argv[2]);
        
        snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_NODES_DB_RELATIVE_PATH);
        nodes_db_t nodes_db(zfilename,true);
        
        std::string inputFileName(argv[1]);

        osmpbf::OSMFileIn inFile(inputFileName, false);

        if (!inFile.open())
        {
            std::cout << "Failed to open " <<  inputFileName << std::endl;
            return -1;
        }

        osmpbf::PrimitiveBlockInputAdaptor pbi;
        while (inFile.parseNextBlock(pbi))
        {
            if (pbi.isNull())
                continue;
            parseBlock_nodes(pbi, elevation_db, nodes_db, ways, ways_ccid, node_ways, lookup);
        }
        
        nodes_db.commit();
    }

    fprintf(stderr,"Lookup db\n");
    
    {
        // lookup_db (write)
        snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
        lookup_db_t lookup_db(zfilename,true);
        
        for(auto lit1 = lookup.begin();
                lit1 != lookup.end();
                lit1++)
        {
            for(auto lit2 = lit1->second.begin();
                    lit2 != lit1->second.end();
                    lit2++)
            {
                lookup_key_t lookup_key;
                lookup_key.lon = lit1->first;
                lookup_key.lat = lit2->first;
                lookup_db.write_area(lookup_key, lit2->second);
            }
        }

        lookup.clear();
        lookup_db.commit();
    }

    RV_BLANKLINE;
	return 0;
}
