/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "read_from_stdin.h"

parameters_t read_parameters_from_stdin()
{
    parameters_t parameters;

    // default values
    parameters.mass = 90;
    parameters.power = 140;
    parameters.SCx = .45;
    parameters.Cr = .008;
    parameters.velocity_nopower = 9.7;
    parameters.velocity_brake = 13.9;
    parameters.velocity_equilibrium = 1.4;
    parameters.criterion = RV_CRITERION_ENERGY;
    parameters.power_walk = 140;
    parameters.lateral_acceleration = .5;
    parameters.walk_penalty = 1;
    parameters.Cw = .03;

    std::string line;

    double velocity_base = 6.95; // 25 km/h

    while(true)
    {
        std::getline(std::cin, line);

        unsigned int k = line.find(' ');
        std::string key = line.substr(0,k);
        std::string val = line.substr(k+1,std::string::npos);

        if(key == "mass")
            parameters.mass = atof(val.c_str());
        else if(key == "velocity_base")
            velocity_base = atof(val.c_str());
        else if(key == "SCx")
            parameters.SCx = atof(val.c_str());
        else if(key == "Cr")
            parameters.Cr = atof(val.c_str());
        else if(key == "velocity_nopower")
            parameters.velocity_nopower = atof(val.c_str());
        else if(key == "velocity_brake")
            parameters.velocity_brake = atof(val.c_str());
        else if(key == "velocity_equilibrium")
            parameters.velocity_equilibrium = atof(val.c_str());
        else if(key == "power_walk")
            parameters.power_walk = atof(val.c_str());
        else if(key == "lateral_acceleration")
            parameters.lateral_acceleration = atof(val.c_str());
        else if(key == "walk_penalty")
            parameters.walk_penalty = atof(val.c_str());
        else if(key == "Cw")
            parameters.Cw = atof(val.c_str());
        else if(key == "criterion")
        {
            if(val == "energy")
                parameters.criterion = RV_CRITERION_ENERGY;
            else if(val == "energy_d")
                parameters.criterion = RV_CRITERION_ENERGY_D;
            else if(val == "time")
                parameters.criterion = RV_CRITERION_TIME;
            else if(val == "time_d")
                parameters.criterion = RV_CRITERION_TIME_D;
            else if(val == "distance")
                parameters.criterion = RV_CRITERION_DISTANCE;
            else
            {
                fprintf(stderr,"Unknown criterion [%s]\n",val.c_str());
                abort();
            }
        }
        else if(key == "points")
            break;
        else
        {
            fprintf(stderr,"Unknown parameter [%s]\n",key.c_str());
            abort();
        }

    }

    parameters.power = 
        .5 * RV_RHO_AIR * parameters.SCx * velocity_base * velocity_base * velocity_base
        +
        parameters.Cr * parameters.mass * RV_G * velocity_base;

    return parameters;
}

std::list<std::pair<double,double>> read_points_from_stdin()
{
    std::list<std::pair<double,double>> points;
    std::string line;
    
    while(true)
    {
        std::getline(std::cin, line);

        unsigned int k = line.find(' ');
        std::string v1 = line.substr(0,k);
        std::string v2 = line.substr(k+1,std::string::npos);
        
        if(v1 == "route")
            break;

        points.push_back(
                std::pair<double,double>(
                    atof(v1.c_str()),
                    atof(v2.c_str())
                    )
                );

    }
    return(points);
}






