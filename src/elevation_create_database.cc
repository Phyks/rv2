/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <list>
#include <libgen.h>
#include <string>
#include <stdexcept>
#include <fstream>
#include <sys/stat.h>

#include "elevation.h"


int main(int argc, char** argv)
{
    if(argc<3)
    {
        fprintf(stderr,"Usage: %s db_path srtm_files...\n",argv[0]);
        abort();
    }

    char zfilename[RV_ZCHAR_LENGTH];
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/data.mdb",argv[1]);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/lock.mdb",argv[1]);
    remove(zfilename);
    mkdir(argv[1],0755);

    elevation db(argv[1],true);
    
    std::list< std::pair<int32_t,int32_t> > missing_data;

    for(int i=2; i<argc; i++)
    {
        std::string filename = basename(argv[i]);
        if(
                filename.size() == 11
                && filename.substr(7) == std::string(".hgt")
                && (filename[0] == 'N' || filename[0] == 'S')
                && (filename[3] == 'E' || filename[3] == 'W')
          )
        {
            int lat,lon;
            try
            {
                lat = std::stoi(filename.substr(1,2));
                lon = std::stoi(filename.substr(4,3));
            }
            catch (const std::invalid_argument& ia)
            {
                fprintf(stderr,"invalid filename %s\n",argv[i]);
                continue;
            }

            if(filename[0] == 'S')
                lat *= -1;
            if(filename[3] == 'W')
                lon *= -1;

            fprintf(stderr,"input file %s for (lon,lat) in [%i,%i[ x [%i,%i[... ",argv[i],lon,lon+1,lat,lat+1);
            std::ifstream ifs(argv[i]);
            if(!ifs.is_open())
            {
                fprintf(stderr,"ERROR openning file\n");
                continue;
            }
            for(int lat_iter_3=3600; lat_iter_3>=0; lat_iter_3-=3)
            {
                for(int lon_iter_3=0; lon_iter_3<=3600; lon_iter_3+=3)
                {
                    if(ifs.eof())
                    {
                        fprintf(stderr,"Unexpected end of file\n");
                        abort();
                    }

                    char c[2];
                    ifs.read(c,2);

                    short s;
                    s=(unsigned char)(c[0]);
                    s=s<<8;
                    s+=(unsigned char)(c[1]);

                    if(lon_iter_3!=3600 && lat_iter_3!=3600)
                    {
                        int32_t lon_s=3600*lon+lon_iter_3;
                        int32_t lat_s=3600*lat+lat_iter_3;
                        if(s!=-32768)
                        {
                            elevation_value* sf = db.grid_point(lon_s, lat_s,true);
                            *sf = s;
                        }
                        else
                        {
                            missing_data.push_back(std::pair<int32_t,int32_t>(lon_s,lat_s));
                            if(lon_s%3!=0 || lat_s%3!=0)
                            {
                                fprintf(stderr,"(init) What the fuck %u %u\n",lon_s,lat_s);
                            }
                        }
                    }
                }
            }
            fprintf(stderr,"done\n");
        }
        else
        {
            fprintf(stderr,"invalid filename %s\n",argv[i]);
        }


    }
    fprintf(stderr,"Commit... ");
    db.commit();
    fprintf(stderr,"done\n");

    fprintf(stderr,"Length of missing data: %lu\n",missing_data.size());
    unsigned int iter = 0;

    unsigned int old_missing_data_size = missing_data.size();
    while(missing_data.size()!=0)
    {
        iter++;
        fprintf(stderr, "Pass %i... ",iter);

        std::list< std::pair< std::pair<int32_t,int32_t>, elevation_value> > new_data;

        std::list< std::pair<int32_t,int32_t> >::iterator missing_data_it = missing_data.begin();
        while(missing_data_it != missing_data.end())
        {
            int32_t lon_s = missing_data_it->first;
            int32_t lat_s = missing_data_it->second;

            if(lon_s%3!=0 || lat_s%3!=0)
            {
                fprintf(stderr,"What the fuck %u %u\n",lon_s,lat_s);
            }

            int32_t lon_s_w = lon_s - 3;
            int32_t lon_s_e = lon_s + 3;
            int32_t lat_s_s = lat_s - 3;
            int32_t lat_s_n = lat_s + 3;

            elevation_value *pneiW, *pneiE, *pneiS, *pneiN;

            pneiE = db.grid_point(lon_s_e, lat_s);
            pneiW = db.grid_point(lon_s_w, lat_s);
            pneiS = db.grid_point(lon_s, lat_s_s);
            pneiN = db.grid_point(lon_s, lat_s_n);

            if( !(!pneiN) + !(!pneiS) + !(!pneiW) + !(!pneiE) >= 2)
            {
                elevation_value sum = 0;
                int n=0;
                if(pneiE)
                {
                    sum += *pneiE;
                    n += 1;
                }
                if(pneiW)
                {
                    sum += *pneiW;
                    n += 1;
                }
                if(pneiS)
                {
                    sum += *pneiS;
                    n += 1;
                }
                if(pneiN)
                {
                    sum += *pneiN;
                    n += 1;
                }
                elevation_value elev = sum/n;

                new_data.push_back(std::pair< std::pair<int32_t,int32_t>, elevation_value>( *missing_data_it, elev));
                std::list< std::pair<int32_t,int32_t> >::iterator missing_data_it_old = missing_data_it;
                missing_data_it++;
                missing_data.erase(missing_data_it_old);
            }
            else
                missing_data_it++;
        }

        fprintf(stderr,"writing... ");
        for(
                std::list< std::pair< std::pair<int32_t,int32_t>, elevation_value> >::iterator new_data_iter = new_data.begin();
                new_data_iter!=new_data.end();
                new_data_iter++
           )
        {
            elevation_value* pev = db.grid_point( 
                                        (*new_data_iter).first.first,
                                        (*new_data_iter).first.second,
                                        true);
            *pev = (*new_data_iter).second;
        }

        fprintf(stderr,"commit... ");
        db.commit();
        fprintf(stderr,"done\n");
        fprintf(stderr,"Length of missing data: %lu\n",missing_data.size());
        if(old_missing_data_size == missing_data.size())
        {
            fprintf(stderr,"Erreur\n");
            for(std::list<std::pair<int32_t,int32_t> >::iterator m = missing_data.begin();m!=missing_data.end();m++)
            {
                fprintf(stderr,"%u\t%u\n",m->first,m->second);
            }
            abort();
        }

        old_missing_data_size = missing_data.size();
    }

    
    return(0);
}
