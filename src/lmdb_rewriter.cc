/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <lmdb.h>
#include "conf.h"
#include <sys/stat.h>



int main(int argc, char ** argv)
{

    if(argc<2)
    {
        fprintf(stderr,"Udage: %s original_db_path/ copy_db_path/\n",argv[0]);
        abort();
    }

    char zfilename[RV_ZCHAR_LENGTH];
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/data.mdb",argv[2]);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/lock.mdb",argv[2]);
    remove(zfilename);
    mkdir(argv[2],0755);

    int rc;

    MDB_env *env1;
    MDB_dbi dbi1;
    MDB_txn *txn1;
    MDB_cursor *cursor1;

    rc = mdb_env_create(&env1);
    if(rc)
    {
        fprintf(stderr, "mdb_env_create: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_set_mapsize(env1, RV_MAXIMUM_LMDB_SIZE);
    if(rc)
    {
        fprintf(stderr, "mdb_env_set_mapsize: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_open(env1, argv[1], 0, 0644);
    if(rc)
    {
        fprintf(stderr, "mdb_env_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_txn_begin(env1, NULL, MDB_RDONLY, &txn1);
    if(rc)
    {
        fprintf(stderr, "mdb_txn_begin: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_dbi_open(txn1, NULL, 0, &dbi1);
    if(rc)
    {
        fprintf(stderr, "mdb_dbi_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }


    MDB_env *env2;
    MDB_dbi dbi2;
    MDB_txn *txn2;

    rc = mdb_env_create(&env2);
    if(rc)
    {
        fprintf(stderr, "mdb_env_create: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_set_mapsize(env2, RV_MAXIMUM_LMDB_SIZE);
    if(rc)
    {
        fprintf(stderr, "mdb_env_set_mapsize: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_open(env2, argv[2], 0, 0644);
    if(rc)
    {
        fprintf(stderr, "mdb_env_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_txn_begin(env2, NULL, 0, &txn2);
    if(rc)
    {
        fprintf(stderr, "mdb_txn_begin: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_dbi_open(txn2, NULL, 0, &dbi2);
    if(rc)
    {
        fprintf(stderr, "mdb_dbi_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }
    
    MDB_val key, data;


    rc = mdb_cursor_open(txn1, dbi1, &cursor1);
    while ((rc = mdb_cursor_get(cursor1, &key, &data, MDB_NEXT)) == 0)
    {
        rc = mdb_put(txn2, dbi2, &key, &data, 0);
        if(rc)
        {
            fprintf(stderr, "mdb_put: (%d) %s\n", rc, mdb_strerror(rc));
            abort();
        }
    }
    mdb_cursor_close(cursor1);
    mdb_txn_abort(txn1);
    mdb_close(env1, dbi1);
    mdb_env_close(env1);

    mdb_txn_commit(txn2);
    mdb_close(env2, dbi2);
    mdb_env_close(env2);

    return 0;
}
