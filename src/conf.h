/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef H_CONF
#define H_CONF 1

#define RV_MAXIMUM_LMDB_SIZE 1099511627776L // 1 TiB
#define RV_ZCHAR_LENGTH 2048 // I love big numbers
#define RV_TEMP_TABLES_PREFIX "rv_temp_"
#define RV_NODES_DB_RELATIVE_PATH "nodes"
#define RV_LOOKUP_DB_RELATIVE_PATH "lookup"
#define RV_LOOKUP_PREC 100

#define RV_ERROR_NO_NODE_FOUND 10

#define RV_BLANKLINE fprintf(stderr,"\r%79s\r","")

#define RV_PI 3.141592653589793
#define RV_EARTH_RADIUS 6371000.0 // m
#define RV_G 9.81 // m.s^-2
#define RV_RHO_AIR 1.204 // kg.m^-3


#define RV_ELEVATION_SQ_LEN 300 // modulo(1200,RV_ELEVATION_SQ_LEN) must be 0
#define RV_POSTGRES_PIPELINE_RETAIN 0

#define RV_MAX_PIPELINE 128

#define RV_WIDTH_CYCLEWAY 1.5
#define RV_WIDTH_OTHERS 2.5

#define RV_MAXIMUM_USED_ANGLE 3 // rad
#define RV_MAXIMUM_RADIUS_OF_CURVATURE 500 // m

#define RV_STATUS_SCALE 1000

#endif
